set terminal png background '#000000' font "Liberation 12"
set border lc rgb '#fbffff'
set border linewidth 1.5
set linetype 1 lc rgb '#86b6dc' lw 1.5
set linetype 2 lc rgb '#b3e8dc' lw 1.5
set linetype 3 lc rgb '#fff1c9' lw 1.5
set linetype 4 lc rgb '#a4f0ff' lw 1.5
set linetype 5 lc rgb '#ccf3ff' lw 1.5
set linetype 6 lc rgb '#d8fdff' lw 1.5
set linetype cycle 6
set key textcolor '#fbffff'
set title textcolor '#fbffff'
set xlabel textcolor '#fbffff'
set ylabel textcolor '#fbffff'
set output ARG1

! ./pi 25 > tmp.dat

f(x) = abs(x-pi)

set logscale y
set title "Convergence of series approximations to pi"
set xlabel "iterations"
set ylabel "discrepancy"

plot[0:26] "tmp.dat" using 1:(f(column(2))) title "arithmetic-geometric mean" with linespoints, \
           "tmp.dat" using 1:(f(column(3))) title "atan {4,1/2,4,1/5,4,1/8}" with linespoints, \
           "tmp.dat" using 1:(f(column(4))) title "atan {16,1/5,-4,1/239}" with linespoints, \
           "tmp.dat" using 1:(f(column(5))) title "atan {24,1/8,8,1/57,4,1/239}" with linespoints

unset logscale
