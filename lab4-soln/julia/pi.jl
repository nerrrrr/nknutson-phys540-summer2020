using Printf

function atan_series(x,N)
   x2 = x*x
   val = 0.0
   m = 2*N+1
   for n in N:-1:0
      val = val*x2
      if n%2 == 0
         val += 1.0/m
      else
         val -= 1.0/m
      end
      m = m-2
   end
   return val*x
end

let
maxIterations = 0
if length(ARGS) != 1
   println("Usage: pi #iterations:")
elseif parse(Int,ARGS[1]) < 1
   println("You must enter a postive number of iterations")
else
   maxInterations = parse(Int,ARGS[1])
   counter_width = 2
   N = maxIterations
   while N != 0
      N /= 10
      counter_width += 1
   end
   for n in 1..maxIterations
      @printf("%d %f\n",n,16*atan_series(0.2,n) - 4*atan_series(1.0/239,n))
   end
end
end
