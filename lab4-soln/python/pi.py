import sys
import math

def atan_series(x,N):
    x2 = x*x
    val = 0.0
    n = N
    m = 2*N+1
    while n >= 0:
        val *= x2
        if n%2 == 0:
            val += 1.0/m
        else:
            val -= 1.0/m
        n -= 1
        m -= 2
    return val*x

if len(sys.argv) != 2:
    raise TypeError("Must provide one command line argument.")
max_iterations = int(sys.argv[1])
if max_iterations < 1:
    raise ValueError("Must pass a positive number.")
counter_width = 2
N = max_iterations
while N != 0:
    N //= 10
    counter_width += 1

fmt_str = "{:"+str(counter_width)+"d}{:20.13f}"
for n in range(1,max_iterations+1):
    print(fmt_str.format(n,16*atan_series(0.2,n) - 4*atan_series(1.0/239,n)))

print("-"*(counter_width+20))
fmt_str = "{:>"+str(counter_width)+"}{:20.13f}"
print(fmt_str.format("inf",math.pi))
