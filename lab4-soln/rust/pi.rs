use std::env;

fn atan_series(x:f64, n:i32) -> f64 {
    let x2 = x*x;
    let mut val = 0.0;
    let mut m = 2*n+1;
    let mut nn = n;
    while nn >= 0 {
       val *= x2;
       if nn%2 == 0 {
           val += 1.0/(m as f64);
       }
       else {
           val -= 1.0/(m as f64);
        }
       nn -= 1;
       m -= 2;
    }
    return val*x
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
       eprint!("Usage: pi #iterations");
       return;
    }
    let max_iterations: i32 = args[1].parse().unwrap_or(0);
    let mut counter_width = 2;
    {
        let mut n = max_iterations;
        while n != 0 {
            n /= 10;
            counter_width += 1;
        }
    }

    for n in 1..(max_iterations+1) {
        println!("{:width$}{:20.13}", n, 16.0*atan_series(0.2,n) - 4.0*atan_series(1.0/239.0,n), width=counter_width);
    }

    println!("{}","-".repeat(counter_width+20));
    println!("{:>width$}{:20.13}","inf",std::f64::consts::PI, width=counter_width);
}
