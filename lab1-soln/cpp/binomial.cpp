#include <iostream>
using std::cout;
using std::endl;
using std::max;
using std::min;

unsigned long int factorial(unsigned long int n)
{
   if (n == 0 or n == 1)
      return 1ul;
   else
   {
      unsigned long int val = 2ul;
      for (unsigned long int m = 3; m <= n; ++m)
         val *= m;
      return val;
   }
}

unsigned long int binomial(unsigned long int n, unsigned long int k)
{
   return factorial(n)/factorial(k)/factorial(n-k);
}


unsigned long int new_binomial(unsigned long int n, unsigned long int k)
{
  unsigned long int val = 1;

// Numerator Computation
  for(int i = n; i > max(k,n-k); i--)
  {
    val*=i;
  }

// Denominator Computation
  for(int i = 1; i <= min(k,n-k); i++)
  {
    val/=i;
  }

  return val;
}

int main(int argc, char *argv[])
{
  unsigned long int n = atoi(argv[1]);

  for (unsigned long int k = 0; k <= n; ++k)
  {
    cout << "(" << n << " choose " << k << ") = " << new_binomial(n,k) << endl;
  }

/*
   for (unsigned long int k = 0; k <= n; ++k)
   {
      cout << "(" << n << " choose " << k << ") = " << binomial(n,k) << endl;
   }
   return 0;
*/
}

