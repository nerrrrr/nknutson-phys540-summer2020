#include <cassert>

#include <iostream>
using std::cout;
using std::endl;

int main()
{
  int a = 1;
  int b = 0;
  int index = 0;

  while(b < a)
  {
    cout << "2^" << index <<  " = " << a << endl;
    index++;
    b = a;
    a = a*2;
    cout << "-1+2^" << index << " = " << a-1 << endl;
  }

/*
   assert(a == 1);
   cout << "2^0 = " << a << endl;
   a = a*2;

   assert(a == 2);
   cout << "2^1 = " << a << endl;
   a = a*2;

   assert(a == 2*2);
   cout << "2^2 = " << a << endl;
   a = a*2;

   assert(a == 2*2*2);
   cout << "2^3 = " << a << endl;
   a = a*2;

   cout << "2^4 = " << a << endl;
   a = a*2;

   cout << "2^5 = " << a << endl;
   a = a*2;
*/

   return 0;
}

