a = 1;
b = a;
index = 0;

while b <= a
  println("2^",index," = ", a);
  global index += 1;
  global b = a;
  global a = a*2;
  println("-1+2^",index," = ", a-1);
end
#=
a = 1;
println("2^0 = ", a);

a = a*2
println("2^1 = ", a);

a = a*2
println("2^3 = ", a);

a = a*2
println("2^4 = ", a);

a = a*2
println("2^5 = ", a);
=#
