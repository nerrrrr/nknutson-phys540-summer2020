function factorial(n)
   if n == 0 || n == 1
      return 1
   else
      let
         val = 2
         for m in 3:n
            val *= m;
         end
         return val
      end
   end
end

function binomial(n,k)
   return numerator(factorial(n)//factorial(k)//factorial(n-k))
end

function new_binomial(n,k)
  let
    val = 1
      for i in max(k,n-k)+1:n   #=  Numerator Calculation =#
        val *= i
      end
      for i in 1:min(k,n-k)     #=  Denominator Calculation =#
        val /= i
      end
      return convert(Int,val)
#=  I wanted to return integers instead of floats =#
  end
end

n = parse(Int,ARGS[1])
for k in 0:n
   println("(",n," choose ",k,") = ", new_binomial(n,k))
end

#=
n = parse(Int,ARGS[1])
for k in 0:n
   println("(",n," choose ",k,") = ", binomial(n,k))
end
=#
