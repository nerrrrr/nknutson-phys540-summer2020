/*
fn factorial(n:u32) -> u32 {
    if n == 0 || n == 1 {
        1
    } else {
        let mut val:u32 = 2;
        for m in 3..=n {
            val *= m;
        }
        val
    }
}

fn binomial(n:u32,k:u32) -> u32 {
   factorial(n)/factorial(k)/factorial(n-k)
}
*/
fn new_binomial(n:u32,k:u32) -> u32 {
    let mut val:u32 = 1;

    for i in 1..=n-std::cmp::max(k,n-k)
    {
        val *= i+std::cmp::max(k,n-k);
        if i <= std::cmp::min(k,n-k)
        {
            val /= i;
        }
    }
/*
    for i in 1..=n {
        if i < std::cmp::min(k,n-k)
        {
            val /=i;
        }
        if i > std::cmp::max(k,n-k)
        {
            val *= i;
        }
    }
*/
    return val;
}

fn main()
{
    let n:u32 = std::env::args().nth(1)
        .expect("Specify which row of Pascal's triangle to compute.")
        .parse()
        .expect("Expecting a single postive integer argument.");
    for k in 0..=n {
      println!("({} choose {}) = {}", n, k, new_binomial(n,k));
   }
/*
    for k in 0..=n {
      println!("({} choose {}) = {}", n, k, binomial(n,k));
   }
*/
}

