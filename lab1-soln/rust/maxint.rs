fn main() {
    let mut a = 1;
    let mut b = 0;
    let mut index =0;

    while b < a
    {
        println!("2^{} = {}", index, a);
        b = a;
        index+=1;
        a=2*a;
        println!("-1+2^{} = {}", index, a-1);
    }
/*

    assert!(a == 1);
    println!("2^0 = {}", a);
    a = a*2;

    assert!(a == 2);
    println!("2^1 = {}", a);
    a = a*2;

    assert!(a == 2*2);
    println!("2^2 = {}", a);
    a = a*2;

    assert!(a == 2*2*2);
    println!("2^3 = {}", a);
    a = a*2;

    println!("2^4 = {}", a);
    a = a*2;

    println!("2^5 = {}", a);
*/
}
