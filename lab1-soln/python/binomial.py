import sys

def factorial(n):
    if n == 0 or n == 1:
        return 1
    else:
        val = 2
        for m in range(3,n+1):
            val *= m
        return val

def binomial(n,k):
    return factorial(n)//factorial(k)//factorial(n-k)

def new_binomial(n,k):
    val = 1
    for i in range(max(k,n-k)+1,n+1):
        val *= i
    for i in range(1,min(k,n-k)+1):
        val /= i
    return val

n = int(sys.argv[1])
for k in range(n+1):
   print("("+str(n)+" choose "+str(k)+") =", new_binomial(n,k))

"""
n = int(sys.argv[1])
for k in range(n+1):
   print("("+str(n)+" choose "+str(k)+") =", binomial(n,k))
"""
