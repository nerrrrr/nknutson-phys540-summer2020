"
Use this program as:
    python maxint.py <integer>
"
import sys

a=1
b=0
index=0
cap = int(sys.argv[1])
while b < a and index < cap :
    print("2^",index," = ", a);
    b=a
    index+=1
    a=a*2
"""
a = 1
print("2^0 =", a);

a = a*2
print("2^1 =", a);

a = a*2
print("2^3 =", a);

a = a*2
print("2^4 =", a);

a = a*2
print("2^5 =", a);
"""
