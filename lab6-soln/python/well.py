import math
import sys

if len(sys.argv) != 2:
    print("Usage: python well.py <gamma>")
    exit(1)

m = 1.0 # kg
K = 1.0 # N/m
omega = math.sqrt(K/m) # radians/s
freq = omega/(2.0*math.pi) # Hz
period = (2.0*math.pi)/omega # s
dt = period/200.0
N = 5000 # total number of time steps
runtime = N*dt

# New variables
gamma = float(sys.argv[1])
k_BT = 0.5

print("runtime =", runtime, file=sys.stderr)
print(" period =", period, file=sys.stderr)
print("   freq =", freq, file=sys.stderr)
print("", file=sys.stderr)

traj = []

# quadratic potential energy
def V(x):
    return 0.5*K*x*x

# linear restoring force
def F(x):
    return -K*x

# total energy
def energy(x,v):
    return 0.5*m*v*v + V(x)

# definitions from Langevin Equation section
def b():
    return math.exp(-0.5*gamma*dt/m)
def beta():
    return math.sqrt(2*gamma*k_BT*dt)*RN()

seed = 326694129
M = 10672869179144828629

def BBS():
    global M
    global seed
    # ksdb: avoid exponentiation when possible
    # seed = (seed**2)%M
    seed = (seed*seed)%M
    #
    return seed & 1

def R():
    out = 0.0
    # ksdb: 
    # for i in range(1,53):
    #     out += BBS()/2**(53-i)
    s = 0.5
    for i in range(52):
        out += BBS()*s
        s *= 0.5
    #
    return out

def RN():
    out = 0.0
    return math.sqrt(-2*math.log(R()))*math.cos(2*math.pi*R())


print("Running simulation...", file=sys.stderr)
x = 1.0 # m
v = 0.0 # m/s, released from rest
for n in range(N):
    a = F(x)/m
    beta_next = beta()
    x_next = x + v*dt + 0.5*a*dt*dt*b() + 0.5*b()*dt*beta_next/m
    a_next = F(x_next)/m
    v_next = v + 0.5*(a+a_next)*dt - gamma*(x_next-x)/m + beta_next/m
    x = x_next
    v = v_next
    print("{:10.6f} {:10.6f} {:10.6f} {:10.6f}".format(n*dt,x,v,energy(x,v)))
    traj.append(x)
print("Done.", file=sys.stderr)

print("")
print("")

print("Computing discrete Fourier Transforms...", file=sys.stderr)

cosFT_traj = []
for j in range(N):
    if (j+1)%500 == 0:
        print(j+1,"/",N, file=sys.stderr)
    sum = 0.0
    for k in range(N):
        sum += math.cos(2.0*math.pi*j*k/N)*traj[k]
    cosFT_traj.append(sum)
print("Done cos.", file=sys.stderr)

sinFT_traj = []
for j in range(N):
    if (j+1)%500 == 0:
        print(j+1,"/",N, file=sys.stderr)
    sum = 0.0
    for k in range(N):
        sum += math.sin(2.0*math.pi*j*k/N)*traj[k]
    sinFT_traj.append(sum)
print("Done sin.", file=sys.stderr)


for j in range(N):
    print("{:10.6f} {:10.6f} {:10.6f}".format(j/runtime,cosFT_traj[j],sinFT_traj[j]))
