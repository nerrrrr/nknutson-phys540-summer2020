set terminal png background '#000000' font "Liberation 12"
set border lc rgb '#fbffff'
set border linewidth 1.5
set linetype 1 lc rgb '#86b6dc' lw 1.5
set linetype 2 lc rgb '#b3e8dc' lw 1.5
set linetype 3 lc rgb '#fff1c9' lw 1.5
set linetype 4 lc rgb '#a4f0ff' lw 1.5
set linetype 5 lc rgb '#ccf3ff' lw 1.5
set linetype 6 lc rgb '#d8fdff' lw 1.5
set linetype cycle 6
set key textcolor '#fbffff'
set title textcolor '#fbffff'
set xlabel textcolor '#fbffff'
set ylabel textcolor '#fbffff'

! test -e well.dat || python3 well.py > well.dat

set output "gamma(0.00).png"
plot[0:2] \
  "well.dat" index 1 using 1:2 w l title "cos", \
  "" i 1 u 1:3 w l title "sin"

set output "gamma(0.01).png"
plot[0:2] \
  "well.dat" index 3 using 1:2 w l title "cos", \
  "" i 3 u 1:3 w l title "sin"

set output "gamma(0.10).png"
plot[0:2] \
  "well.dat" index 5 using 1:2 w l title "cos", \
  "" i 5 u 1:3 w l title "sin"

set output "gamma(1.00).png"
plot[0:2] \
  "well.dat" index 7 using 1:2 w l title "cos", \
  "" i 7 u 1:3 w l title "sin"
