// first-order polynomial
fn _polynom1(a:f64, b:f64, x:f64) -> f64 {
   return a*x + b;
}

// second-order polynomial
fn polynom2( a:f64,  b:f64,  c:f64,  x:f64) -> f64 {
   return (a*x + b)*x + c;
}

// third-order polynomial
fn _polynom3( a:f64,  b:f64,  c:f64,  d:f64,  x:f64) -> f64 {
   return ((a*x + b)*x + c)*x + d;
   // why this rather than "return a*x*x*x + b*x*x + c*x + d"?
   // Hint: count the number of operators
}

fn main() {
    for i in -50..51 {
		let x = (i as f64)*0.2;
        // It's necessary to use float literals here:
		println!("{:5.1} {:9.2}", x, polynom2(1.0,2.0,3.0,x));
        // A call to polynom2(1,2,3,x) wouldn't work.
	}
}
