using Printf

function polynom(a, b, x)
	a*x + b
end

function polynom(a, b, c, x)
	(a*x + b)*x + c
end

function polynom(a, b, c, d, x)
	((a*x + b)*x + c)*x + d
	# why this rather than "return a*x*x*x + b*x*x + c*x + d"?
	# Hint: count the number of operators
end

for i in -50:50
	let 
		x = i*0.2
  	@printf("%5.1f  %8.2f\n", x, polynom(1,2,3,x))
	end
end
