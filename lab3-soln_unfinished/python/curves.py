# Python does not support function overloading. The first-, second-,
# and third-order polynomial functions must all have different names.

def polynom1(a, b, x):
    return a*x + b

def polynom2(a, b, c, x):
    return (a*x + b)*x + c

def polynom3(a, b, c, d, x):
    return ((a*x + b)*x + c)*x + d
    # why this rather than "return a*x*x*x + b*x*x + c*x + d"?
    # Hint: count the number of operators

for i in range(101):
    x = (i-50)*0.2
    print("{:5.1f}    {:6.2f}".format(x, polynom2(1,2,3,x)))

