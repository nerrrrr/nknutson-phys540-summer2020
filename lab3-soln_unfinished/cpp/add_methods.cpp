#include <cassert>

#include <vector>
using std::vector;

#include <algorithm>
using std::sort;

#include <numeric>
using std::accumulate;

float add_pairwise(const vector<float> &v)
{
	// add your code here
	
	// replace the following statement with the correct return value
	return 0.0F;
}

float add_increasing(const vector<float> &v)
{
	// add your code here
	
	// replace the following statement with the correct return value
	return 0.0F;
}

float add_decreasing(const vector<float> &v)
{
	// add your code here
	
	// replace the following statement with the correct return value
	return 0.0F;
}

float add_conventional(const vector<float> &v)
{
	return accumulate(v.begin(),v.end(),0.0F);
	// last line is equivalent to the following:
	// float sum = v[0];
	// for (int i = 1; i < v.size(); ++i)
	//    sum += v[i];
	// return sum;
}

float add_compensated(const vector<float> &v)
{
	// add your code here
	
	// replace the following statement with the correct return value
	return 0.0F;
}

float add_high_precision(const vector<float> &v)
{
	// add your code here
	
	// replace the following statement with the correct return value
	return 0.0F;
}

