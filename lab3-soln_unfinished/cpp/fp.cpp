#include <iostream>
using std::cout;
using std::endl;

template <typename T>
void report(T a, char op, T b, T c)
{
   cout << a << " " << op << " " << b << " = " << c << endl;
}

int main()
{
   const float one = 1.0F;
   const float neg_one = -1.0F;
   float zero = one; --zero; // this tricks the compiler

   cout << "Floating point division:" << endl;

   report(one,'/',zero,one/zero);
   report(neg_one,'/',zero,neg_one/zero);

   return 0;
}
