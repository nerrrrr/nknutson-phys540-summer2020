for n_2 in 1:20
  for n_1 in 1:n_2-1
    let
      Delta_energy = -13.6(1/n_2^2 - 1/n_1^2)
      wavelength = 1240/Delta_energy
      allowed_count = 0
      for l_2 in 0:n_2-1
        for l_1 in 0:n_1-1
          for m_2 in -l_2:l_2
            for m_1 in -l_1:l_1
              if abs(l_2 - l_1) == 1 && (abs(m_2 - m_1) == 0 || abs(m_2 - m_1) == 1)
                allowed_count += 1
              end
            end
          end
        end
      end
      if wavelength > 380 && wavelength < 750
        println(n_2,"->",n_1,"v\t",allowed_count,"\t",round.(Delta_energy,sigdigits=3),"\t",round.(wavelength,sigdigits=6))
      else
        println(n_2,"->",n_1,"\t",allowed_count,"\t",round.(Delta_energy,sigdigits=3),"\t",round.(wavelength,sigdigits=6))
      end
    end
  end
end
