for N in 2:10
  let
    count = 0
    for k in 2:N
      for j in 1:(k-1)
        for i in 0:(j-1)
          count += 1
        end
      end
    end
    println(N,"\t",count)
  end
end

#=
let
	N = 6
	for k in 2:N
		for j in 1:(k-1)
			for i in 0:(j-1)
				println("(",i,",",j,",",k,")");
			end
		end
	end
end
=#
