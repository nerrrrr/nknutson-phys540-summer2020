for N in 0:99
  let
    count = 0
    square_count = 0
    root = 0
    sum = 0
    for l in 0:N
      for k in 0:l
        for j in 0:k
          for i in 0:j
            count += 1
            sum = i^2 + j^2 + k^2 + l^2
            root = Int(floor(sqrt(sum)))
            if root^2 == sum
              square_count += 1
            end
          end
        end
      end
    end
    println(N,"\t",count,"\t",square_count)
  end
end

#=
let
	N = 6
	for k in 2:N
		for j in 1:(k-1)
			for i in 0:(j-1)
				println("(",i,",",j,",",k,")");
			end
		end
	end
end
=#
