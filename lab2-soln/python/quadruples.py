import math
for N in range(0,100):
    count = 0
    square_count = 0
    for l in range(0,N+1):
        for k in range(0,l+1):
            for j in range(0,k+1):
                for i in range(0,j+1):
                    squares = i*i + j*j + k*k + l*l
                    root = math.floor(math.sqrt(squares))
                    if root*root == squares:
                        square_count+=1
                    count+=1
    print("{}\t{}\t{}".format(N,count,square_count))

"""
N = 6
for k in range(2,N+1):
    for j in range(1,k):
        for i in range(0,j):
            print("({},{},{})".format(i,j,k))
"""
