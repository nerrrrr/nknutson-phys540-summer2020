set fit errorvariables

e(x) = e0 + e1*x + e2*x**2 + e3*x**3
f(x) = f0 + f1*x + f2*x**2 + f3*x**3 + f4*x**4

fit e(x) "quad.dat" using 1:3 via e0,e1,e2,e3
fit f(x) "quad.dat" using 1:3 via f0,f1,f2,f3,f4

set xlabel "N"
set ylabel "number of sums of squared quadruples that are perfect squares"
set logscale y
plot "quad.dat" using 1:3 title "exact", e(x), f(x)
