#include <iostream>
using std::cout;
using std::endl;

#include <iomanip>
using std::setw;

bool valid_l(int l, int n) { return l >= 0 and l < n; }
bool valid_m(int m, int l) { return m >= -l and m <= l; }

//  Tests whether the change in l and change in m are valid
bool valid_Delta_l(int l_1, int l_2) { return l_2 - l_1 == 1 || l_2 - l_1 == -1; }
bool valid_Delta_m(int m_1, int m_2) { return m_2 - m_1 == 1 || m_2 - m_1 == -1 || m_2 - m_1 ==0; }

int main()
{

  int allowed_pathways;
  double Delta_E;
  double wavelength;

//  Loops over n,l, and m for each transition.
  for (int n_2 = 1; n_2 <= 20; ++n_2)
  for (int n_1 = 1; n_1 < n_2; ++n_1)
  {
//  Computing the change in energy and the wavelength of the photon.
    Delta_E = -13.6*(1/( (double)(n_2*n_2) ) - 1/( (double)(n_1*n_1) ) );
    wavelength = 1240/Delta_E;
    allowed_pathways = 0;
    for (int l_2 = 0; valid_l(l_2,n_2); ++l_2)
    for (int l_1 = 0; valid_l(l_1,n_1); ++l_1)
      for (int m_1 = -l_1; valid_m(m_1,l_1); ++m_1)
      for (int m_2 = -l_2; valid_m(m_2,l_2); ++m_2)
      {
        if ( valid_Delta_l(l_2,l_1) && valid_Delta_m(m_2,m_1) )
        {
          allowed_pathways++;   // Increments for valid l and m along with valid changes in l and m
        }
      }

//  Prints
//  Transition(v if wavelength is in visual spectrum)   Number of Allowed Pathways  Change in Energy [eV]  Wavelength of Photon [nm]
    cout << setw(10) << n_2 << "->" << n_1;

    if ( wavelength > 380 and wavelength < 750 )
      cout << "v";

    cout << setw(10) << allowed_pathways
      << setw(15) << Delta_E
      << setw(15) << wavelength << endl;
  }
   // (!) insert your code here
   return 0;
}

