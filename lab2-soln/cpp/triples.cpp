#include <cassert>

#include <iostream>
using std::cout;
using std::endl;

#include <iomanip>
using std::setw;

#include <cmath>
using std::sqrt;

int main()
{
  int count = 0;

//  Loops over the integers N from 2 to 10
  for (int N = 2; N < 11; ++N)
  {
    count = 0;
    for (int k = 2; k <= N; ++k)
    {
      for (int j = 1; j < k; ++j)
        for (int i = 0; i < j; ++i)
          count++;  //  Instead of printing the each triple, it just increments the count.
    }
//  Prints
//  Integer   Number of triples
  cout << setw(5) << N << setw(5) << count << endl;
  }

}
/*
{
   const int N = 6;
   for (int k = 2; k <= N; ++k)
      for (int j = 1; j < k; ++j)
         for (int i = 0; i < j; ++i)
            cout << "(" << i << "," << j << "," << k << ")" << endl;
   return 0;
}
*/
