#include <cassert>

#include <iostream>
using std::cout;
using std::endl;

#include <iomanip>
using std::setw;

#include <cmath>
using std::sqrt;

int main()
{
  int count = 0;
  int square_count = 0;
  int squares = 0;

  for (int N = 0; N < 100; ++N)
  {
    count = 0;
    square_count = 0;
    for (int k = 0; k <= N; ++k)
      for (int j = 0; j <= k; ++j)
        for (int i = 0; i <= j; ++i)
          for (int l = 0; l <= i; ++l)
          {
//  Increments counter for each quadruple
            count++;
            squares = i*i + j*j + k*k + l*l;
//  Tests for squares by comparing the square of the root to the quantity itself
//  This works because casting to (int) rounds a non-whole number down, which when squared would not be equal to the original
            if( (int)sqrt(squares) * (int)sqrt(squares) == squares)
              square_count++;
          }
//  Prints
//  Integer   Number of Weakly Ordered Quadruples   Number of i^2+j^2+k^2+l^2 that are perfect squares
    cout << setw(10) << N << setw(10) << count << setw(10) << square_count << endl;
  }

}
/*
{
   const int N = 6;
   for (int k = 2; k <= N; ++k)
      for (int j = 1; j < k; ++j)
         for (int i = 0; i < j; ++i)
            cout << "(" << i << "," << j << "," << k << ")" << endl;
   return 0;
}
*/
