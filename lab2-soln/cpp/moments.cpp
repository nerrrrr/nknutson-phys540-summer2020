#include <cassert>

#include <iostream>
using std::cout;
using std::endl;

#include <iomanip>
using std::setw;

#include <algorithm>
using std::swap;

const int Nmax = 16;
const int Sshow = 7;
const int Smax = Nmax+1;

int buffer1[Smax];
int buffer2[Smax];

int* last = buffer1;
int* current = buffer2;

void crossbar(void)
{
   cout << "   +";
   for (int i = 0; i < Sshow; ++i)
      cout << "-------";
   cout << "+-------" << endl;
}

unsigned long int verify_singlet(unsigned long int N)
{
   // (!) write the function body
//  Does exactly what the formula says

  unsigned long int val = 1;

  for (int i = N; i > N/2; i--)   //  Note for the binomial calculation
  {                               //  N/2 = N - N/2
    val *= i;                     //  (k = n -k)
  }
  for (int i = 1; i <= N/2; i++)
  {
    val /= i;
  }

  return val/(N/2 + 1);
}

int main()
{
   for (int i = 0; i < Smax; ++i)
      last[i] = current[i] = 0;

   for (int i = 0; i < Sshow; ++i)
      cout << setw(i == 0 ? 11 : 7) << i/2.0;
   cout << setw(7) << "num" << endl;
   crossbar();

   for (int n = 1; n <= Nmax; ++n)
   {
      if (n == 1)
         last[1] = 1;
      else
      {
         // (!) increment elements of current
//  This came from noticing the pattern in the grid
          current[0] = last[1];
          current[n] = 1;
          for(int k = 1; k < n; ++k)
              current[k] = last[k-1] + last[k+1];

         swap(current,last);
         for (int k = 0; k < Smax; ++k)
            current[k] = 0;
      }

      // (!) uncomment the following line
      assert(n%2 == 1 or int(verify_singlet(n)) == last[0]);

      cout << setw(3) << n << "|";
      for (int i = 0; i < Sshow; ++i)
      {
         cout << setw(7);
         if (last[i] == 0)
            cout << ' ';
         else
            cout << last[i];
      }
      unsigned long int num_states = 0;
//  This works because k = 2S from the notes, and we are computing the sum of (2S + 1) * C_S^(N)
        for (int k = 0; k < Smax; ++k)
          num_states += (k + 1) * last[k];

      // (!) accumulate values in num_states
      cout << '|' << setw(7) << num_states << endl;
   }
   crossbar();


   return 0;
}
