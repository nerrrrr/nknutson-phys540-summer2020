let R = 6378.1; // km (radius of the earth)
let r2 = R + 3000.0; // km (apogee)
let r1 = R + 500.0; // km (perogee)
let a = 0.5*(r1 + r2); // semi-major axis
let e = (r2-r1)/(2.0*a); // eccentricity
let mu = 398600.4418; // km^3/s^2
let P = 2*M_PI*sqrt(a*a*a/mu)/3600.; // period in hrs
let dt = P/5075;

fn eccentric_anomaly(M:f64, E0:f64) -> f64
{
   let mut E = E0;
   // (!) fill in the function body to solve E-e*sin(E) = M
   E
}

fn main()
{
   eprintln!("Eccentricity e = {}", e);
   epringln!("Period P = {}", P);

   for n in 1..50000 {
   {
      let t = n*dt;
      let M = 2.0*M_PI*t/P;
      let E = 0.0; // (!) replace with appropriate call to eccentric_anomaly
      let theta = 0.0; // (!) fill in rhs
      let r = 0.0; // (!) fill in rhs
      println!("{:10.6} {:10.6} {:10.6}", t, r, theta);
   }
}
