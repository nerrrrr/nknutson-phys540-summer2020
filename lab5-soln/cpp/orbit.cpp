#include <cassert>

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <cmath>
using std::fabs;
using std::sin;
using std::cos;
using std::tan;
using std::atan2;

const double R = 6378.1; // km (radius of the earth)
const double r2 = R + 3000.0; // km (apogee)
const double r1 = R + 500.0; // km (perogee)
const double a = 0.5*(r1 + r2); // semi-major axis
const double e = (r2-r1)/(2.0*a); // eccentricity
const double mu = 398600.4418; // km^3/s^2
const double P = 2*M_PI*sqrt(a*a*a/mu)/3600.; // period in hrs
const double dt = P/5075;

double eccentric_anomaly(double M, double E0)
{
   double E = E0;
   // (!) fill in the function body to solve E-e*sin(E) = M
	int iterations = 20;
	for (int i = 0; i < iterations; i++)
	{
		E = E - (E - M - e*sin(E))/(1-e*cos(E));
	}
   return E;
}

int main()
{
   cerr << "Eccentricity e = " << e << endl;
   cerr << "Period P = " << P << endl;

	double E = 0.0;

   for (int n = 1; n < 50000; ++n)
   {
      const double t = n*dt;
      const double M = 2.0*M_PI*t/P;
      E = eccentric_anomaly(M,E); // (!) replace with appropriate call to eccentric_anomaly
      const double theta = 2*atan2(sqrt(1+e)*tan(E/2),sqrt(1-e)); // (!) fill in rhs
      const double r = a*(1-e*cos(E)); // (!) fill in rhs
      cout << t << "\t" << r << "\t" << theta << endl;
   }

   return 0;
}
