set terminal png background '#000000' font "Liberation 12"
set border lc rgb '#fbffff'
set border linewidth 1.5
set linetype 1 lc rgb '#86b6dc' lw 1.5
set linetype 2 lc rgb '#b3e8dc' lw 1.5
set linetype 3 lc rgb '#fff1c9' lw 1.5
set linetype 4 lc rgb '#a4f0ff' lw 1.5
set linetype 5 lc rgb '#ccf3ff' lw 1.5
set linetype 6 lc rgb '#d8fdff' lw 1.5
set linetype cycle 6
set key textcolor '#fbffff'
set title textcolor '#fbffff'
set xlabel textcolor '#fbffff'
set ylabel textcolor '#fbffff'

# change "seq" to "jot" on Mac/BSD

! ./root 3 --verbose > sqrt3.dat
! echo >> sqrt3.dat
! echo >> sqrt3.dat
! ./seriesroot 2 -1 $(seq 30) >> sqrt3.dat
! echo >> sqrt3.dat
! echo >> sqrt3.dat
! ./seriesroot 1.5 0.75 $(seq 30) >> sqrt3.dat
! echo >> sqrt3.dat
! echo >> sqrt3.dat
! ./seriesroot 1.75 -0.0625 $(seq 30) >> sqrt3.dat


set title "Expansion of sqrt(a^2+b)"
set xlabel "truncation order of expansion"
set ylabel "series summation"

set output "seriesroot_summation.png"
plot[0:10] "sqrt3.dat" index 0 title "Heron" with linespoints,\
           "sqrt3.dat" index 1 title "sqrt(2^2-1)" with linespoints,\
           "sqrt3.dat" index 2 title "sqrt(1.5^2+0.75)" with linespoints, \
           "sqrt3.dat" index 3 title "sqrt(1.75^2-0.0625)" with linespoints, sqrt(3.0)

pause -1

set logscale y
set ylabel "deviation from sqrt(3)"
set output "seriesroot_deviation.png"

plot "sqrt3.dat" index 0 using 1:(abs(column(2)-sqrt(3.0))) title "Heron" with linespoints,\
     "sqrt3.dat" index 1 using 1:(abs(column(2)-sqrt(3.0))) title "sqrt(2^2-1)" with linespoints,\
     "sqrt3.dat" index 2 using 1:(abs(column(2)-sqrt(3.0))) title "sqrt(1.5^2+0.75)" with linespoints,\
     "sqrt3.dat" index 3 using 1:(abs(column(2)-sqrt(3.0))) title "sqrt(1.75^2-0.0625)" with linespoints

unset logscale
