#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <iomanip>
using std::setw;

void bad()
{
	cerr << "Returns the sqrt(a^2 + b)" << endl;
	cerr << "Usage :\t $ ./seriesroot a b N1 [N2 N3 N4 ...]" << endl;
	cerr << "\t\twith N > 0" << endl;
	exit(1);
}

int main(int argc, char* argv[])
{
	if (argc < 4)
	{
		bad();
	}
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	int index = 3;
	while (index < argc)
	{
		int N = atoi(argv[index]);
		double sum = 0;
		if (N == 1)
		{
			sum = a;
		}
		else if (N < 1)
			bad();
		else
		{
			double term = -b/(4*a*a);
			for (int n = 1; n < N-1; n++)
			{
				sum += term;
				double const ratio = -b*(2*n+1)/(2*a*a*(n+2));
				term *= ratio;
			}
			sum = a + (1+sum)*b/(2*a);
		}
		index++;
		cout.precision(16);
		cout << setw(10) << N << setw(20) << sum << endl;
	}
}
