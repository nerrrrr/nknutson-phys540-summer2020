#include <cstdlib>
using std::exit;
using std::atof;

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <iomanip>
using std::setw;

#include <cmath>
using std::fabs;

#include <string>
using std::string;

bool nearly_equal(double x, double y)
{
	const double separation = fabs(x-y);
	const double eps = 1E-14;
	return separation < fabs(x)*eps and separation < fabs(y)*eps;
}

void bad()
{
	cerr << "Returns the square root of the provided argument:" << endl;
	cerr << "Usage: root # [--verbose]" << endl;
	exit(1);
}

int main(int argc, char* argv[])
{
	if (argc != 2 and argc != 3) bad();
	double a = atof(argv[1]);
	if (a <= 0.0) bad();
	const bool verb = (argc == 3);
	if (verb and string(argv[2]) != "--verbose") bad();

	cout.precision(16);

	double x = 1.0;
	unsigned long int N = 0;
	while (true)
	{
		if (verb) cout << setw(14) << ++N << setw(20) << x << endl;
		const double x_new = 0.5*(x + a/x);
		if (nearly_equal(x_new,x)) break;
		x = x_new;
	}
	if (!verb)
	{
		cout << "Newton's method value: " << x << endl;
		cout << " C Math library value: " << sqrt(a) << endl;
	}

	return 0;
}
