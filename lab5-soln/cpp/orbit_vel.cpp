#include <cassert>

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
#include <iomanip>
using std::setw;

#include <cmath>
using std::fabs;
using std::sin;
using std::cos;
using std::tan;
using std::atan2;

const double R = 6378.1; // km (radius of the earth)
const double r2 = R + 8500.0; // km (apogee)
const double r1 = R + 1000.0; // km (perogee)
const double a = 0.5*(r1 + r2); // semi-major axis
const double e = (r2-r1)/(2.0*a); // eccentricity
const double mu = 398600.4418; // km^3/s^2
const double P = 2*M_PI*sqrt(a*a*a/mu)/3600.; // period in hrs
const double dt = P/5075;

double eccentric_anomaly(double M, double E0)
{
   double E = E0;
   // (!) fill in the function body to solve E-e*sin(E) = M
   // ksdb: It is usually safer to have a convergence check
   //       rather than a fixed number of iterations
	int iterations = 20;
	for (int i = 0; i < iterations; i++)
	{
		E = E - (E - M - e*sin(E))/(1-e*cos(E));
	}
   return E;
}

int main()
{
   cerr << "Eccentricity e = " << e << endl;
   cerr << "Period P = " << P << endl;

	double E = 0.0;

// Because we are doing the symmetric difference finite difference, we
// need the previous and next values for r and theta We do this by
// creating arrays v[] = {past,present,future}

	double r[3], theta[3];

//	Initializing the arrays

for (int i = 0; i < 3; i++)
	{
		const double t = i*dt;
		const double M = 2.0*M_PI*t/P;
		E = eccentric_anomaly(M,E);
		r[i] = a*(1-e*cos(E));
		theta[i] = 2*atan2(sqrt(1+e)*tan(E/2),sqrt(1-e));
	}

// Because the first data point was used in the initialization of the
// arrays, we start at n = 2

   for (int n = 2; n < 50000; ++n)
   {
      double t = n*dt;
      double M = 2.0*M_PI*t/P;

      cout << setw(10) << t
			<< setw(15) << r[1]
			<< setw(15) << theta[1]
			<< setw(15) << (r[2]-r[0])/(2*dt)
			<< setw(15) << (theta[2]-theta[0])/(2*dt) << endl;

      t = t + dt;
      M = 2.0*M_PI*t/P;
      E = eccentric_anomaly(M,E);

      // Increment the {past,present,future}
		r[0] = r[1];
		theta[0] = theta[1];

		r[1] = r[2];
		theta[1] = theta[2];

		r[2] = a*(1-e*cos(E));
		theta[2] = 2*atan2(sqrt(1+e)*tan(E/2),sqrt(1-e));
		while(theta[2] < theta[1])
			theta[2] += 2*M_PI;
   }

   return 0;
}

