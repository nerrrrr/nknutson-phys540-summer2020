! ./orbit_vel > ov.dat

set size square

set title "satellite in earth orbit"

set object 1 rect from -100,-100 to 100,100 fc lt 1 lw 0
set xlabel "x position [km]"
set ylabel "y position [km]"

unset key
plot "ov.dat" using (column(2)*cos(column(3))):(column(2)*sin(column(3))) w l

pause -1


unset object

set xlabel "time [hrs]"
set ylabel "radial velocity [km/hr]"

plot "ov.dat" using 1:4 w l

pause -1


set xlabel "time [hrs]"
set ylabel "angular velocity [radians/hr]"

plot "ov.dat" using 1:5 w l

pause -1


set xlabel "sin(theta)"
set ylabel "total speed [km/hr]"
plot "ov.dat" using (sin(column(3))):(sqrt(column(4)**2 + column(2)**2 * column(5)**2)) w l

pause -1
