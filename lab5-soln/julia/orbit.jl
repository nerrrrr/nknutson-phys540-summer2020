using Printf

R = 6378.1 # km (radius of the earth)
r2 = R + 3000.0 # km (apogee)
r1 = R + 500.0 # km (perogee)
a = 0.5*(r1 + r2) # semi-major axis
e = (r2-r1)/(2.0*a) # eccentricity
mu = 398600.4418 # km^3/s^2
P = 2*M_PI*sqrt(a*a*a/mu)/3600. # period in hrs
dt = P/5075.;

function eccentric_anomaly(M, E0):
    E = E0
    # (!) fill in the function body to solve E-e*sin(E) = M
    return E
 end
 
println("Eccentricity e = $e")
println("Period P = $P")

for n in 1:49999
    t = n*dt;
    M = 2.0*M_PI*t/P;
    E = 0.0 # (!) replace with appropriate call to eccentric_anomaly
    theta = 0.0 # (!) fill in rhs
    r = 0.0 # (!) fill in rhs
    @printf("%10.6f %10.6f %10.6f\n", t, r, theta)
 end
